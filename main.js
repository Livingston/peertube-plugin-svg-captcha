
async function register ({
  registerHook,
  registerSetting,
  settingsManager,
  storageManager,
  videoCategoryManager,
  videoLicenceManager,
  videoLanguageManager,
  peertubeHelpers
}) {
  registerHook({
    target: "filter:api.user.signup.allowed.result",
    handler: (result, params) => verifyCaptcha(result, params, settingsManager)
  })

}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}

async function verifyCaptcha (result, params, settingsManager) {
  if ( ! params || ! params.body ) {
    // We are not on a form submit.
    return result
  }
  
  // TODO
}
